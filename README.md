# CAP_Team

# Running The Software
Compile all java files in the source directory. See the README within 
source/database_setup for information on how to setup the database.

The main GUI program is run with "java runner"

# Barcode Sign In Kiosk
Run with the command "java BarcodeSignIn". The barcode scanner acts as a keyboard,
and so the user must not click away from the console which started the program
or barcode inputs will not be detected.

# Dependencies
All library files needed are located in the source/libraries directory
as .jar files. These need to be added to the java class path before
compiling and running the program.
