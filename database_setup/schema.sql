create database if not exists cap_db;

use cap_db;

#first create the user then grant all privileges one thing to note, you may need to change the IP after the @ symbol depending on what your local IP is this
#can be checked by running ipconfig and finding the ipv4 address for ethernet and put it in the quotes after the @ but leave the last field as a %. 
#this IP will also need to be used inside the Connection in the GUI to connect to the database as well as the fact
#that the user to connect on the LAN is not root but capConnectionHost with the ip that is used on your LAN and the port 3306
CREATE USER 'capConnectionHost'@'10.0.0.%' IDENTIFIED BY 'testing' REQUIRE X509;
GRANT ALL PRIVILEGES ON *.* TO 'capConnectionHost'@'10.0.0.%' WITH GRANT OPTION;



create table CAP_member 
    (CAP_ID        	varchar(7) not null,
    name            text,
    qualifications  text,
    ec_name         text,
    ec_phonenum     text,
    primary key (CAP_ID));

create table mission
    (mission_id     int,
    incident_name   text,
    start_datetime  text,
    end_datetime    text,
    in_progress     Boolean,
    primary key (mission_id));

create table guest_attendee
    (name           varchar(766),
    ec_name         text,
    ec_phonenum     text,
    mission_id      int,
    signin_time     text,
    signout_time    text,
    primary key (name, mission_id));

create table member_attendee
    (CAP_ID        	varchar(7) not null,
    mission_id      int,
    signin_time     varchar(700),
    signout_time    text,
    primary key (CAP_ID, mission_id, signin_time));
