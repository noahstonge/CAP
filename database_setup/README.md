# Database Setup

This database is designed to use MySQL and will be connected to through
Java using JDBC.

MySQL downloads can be found here:
https://dev.mysql.com/doc/mysql-getting-started/en/

To initialize database, open MySQL and run `source schema.sql;`


Depending upon the IP class of the local network, setting up a secure database with only LAN access requires configuration. First off port 3306 should be open on the local machine's firewall that hosts the database as this port is used by MySQL. Next up depending on the machines local network class we will need to edit the schema.sql file.

First we should check the IP of the machine hosting the database this can be done on windows using ipconfig. 
Take note of the first three values of the local IPv4 address for your internet adapter (either Ethernet or Wifi).


Once we find the local IP (Either 10.X.X.X or 172.X.X.X or 192.X.X.X)
We should go into the schema.sql file and change in lines 9 and 10 after the @ symbol inside the '' we should replace the First
three numbers on both lines inside the '' with the first three numbers found in your local IP address that we found above, be careful not
to replace the % value and not to input more than the first 3 values.


Lastly when setting up the database connection on MySQL the hostname should be the full local IPv4 that we found above and port should be 3306.


Steps for configuring encryption:
Copy the server certificate and key from `cap_team/certs` to the mysql data directory on the server.
On Linux this is located at: `/var/lib/mysql`
The client needs access to `trustore` and `keystore` located in `cap_team/certs`
For final deployment, new certificates should be generated and distributed through a secure channel
Certificates were generated using mysql as detailed here:
https://dev.mysql.com/doc/refman/8.0/en/mysql-ssl-rsa-setup.html
Once those certificates are generated, a new keystore and trustore needs to be generated as detailed here:
https://dev.mysql.com/doc/connector-j/8.0/en/connector-j-reference-using-ssl.html
