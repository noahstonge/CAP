/* ModelTest.java
 *
 * Unit tests for Model.java
 * These tests are meant to be run on a database which has already
 * been initialized, but is currently empty.
 *
 * Returns database to it's empty state upon successfull completion
 */

import java.time.LocalDate;
import java.time.LocalTime;

import java.util.List;
import java.util.ArrayList;

public class ModelTest {
    Model model;
    Model badModel;

    public ModelTest(String url, String user, String password) {
        // This model can communicate w/ database
        DatabaseConnection dbct = new DatabaseConnection(url, user, password);
        dbct.printStackTrace = true;
        model = new Model(dbct);

        // This model should always get an error when communicating with db
        dbct = new DatabaseConnection(url, user, password + "1");
        badModel = new Model(dbct);
    }

    public boolean runAll() {
        ModelState res;

        // Tests to verify that failed database communications return correct value
        if (!runDbComFailTests()) {
            return false;
        }

        // Run tests for general model functionality
        if (!runGeneralTests()) {
            return false;
        }

        return true;
    }

    private boolean runDbComFailTests() {
        ModelState res;

        Mission m = new Mission();
        m.missionId = 1;

        CAPMember member1 = new CAPMember();
        member1.capId = "123456";
        member1.qualifications = "none";
        member1.name = "Bob Smith";
        member1.ecName = "Alice Jones";
        member1.ecPhone = "(111)222-3334";

        Guest guest1 = new Guest();
        guest1.name = "John Johnson";
        guest1.ecName = "Alice Jones";
        guest1.ecPhone = "1112223334";

        List retList = null;

        // Tests to verify that.1 FAILED database communications return correct value
        res = badModel.checkConnection();
        if (res != ModelState.ERROR_DB_COM_FAILED) {
            System.out.println("MODEL TEST 1.1 FAILED");
            return false;
        }

        res = badModel.isMissionInProgress();
        if (res != ModelState.ERROR_DB_COM_FAILED) {
            System.out.println("MODEL TEST 2.1 FAILED");
            return false;
        }

        res = badModel.startMission("Test Mission");
        if (res != ModelState.ERROR_DB_COM_FAILED) {
            System.out.println("MODEL TEST 3.1 FAILED");
            return false;
        }

        res = badModel.endMission();
        if (res != ModelState.ERROR_DB_COM_FAILED) {
            System.out.println("MODEL TEST 4.1 FAILED");
            return false;
        }

        GetAllMissionsRet resMissions = badModel.getAllMissions();
        if (resMissions.state != ModelState.ERROR_DB_COM_FAILED) {
            System.out.println("MODEL TEST 5.1 FAILED");
            return false;
        }

        res = badModel.deleteMission(m);
        if (res != ModelState.ERROR_DB_COM_FAILED) {
            System.out.println("MODEL TEST 6.1 FAILED");
            return false;
        }

        res = badModel.generateForm211(m);
        if (res != ModelState.ERROR_DB_COM_FAILED) {
            System.out.println("MODEL TEST 7.1 FAILED");
            return false;
        }

        GetAttendeesRet resAttendees = badModel.getAttendees(m);
        if (resAttendees.state != ModelState.ERROR_DB_COM_FAILED) {
            System.out.println("MODEL TEST 8.1 FAILED");
            return false;
        }

        res = badModel.addCAPMember(member1);
        if (res != ModelState.ERROR_DB_COM_FAILED) {
            System.out.println("MODEL TEST 9.1 FAILED");
            return false;
        }

        member1.ecName = "Alice Smith";

        res = badModel.editCAPInfo(member1);
        if (res != ModelState.ERROR_DB_COM_FAILED) {
            System.out.println("MODEL TEST 10.1 FAILED");
            return false;
        }

        GetCAPMemberRet resCAP = badModel.getCAPMember("123456");
        if (res != ModelState.ERROR_DB_COM_FAILED) {
            System.out.println("MODEL TEST 11.1 FAILED");
            return false;
        }

        res = badModel.CAPMemberSignIn(member1.capId, "TIME");
        if (res != ModelState.ERROR_DB_COM_FAILED) {
            System.out.println("MODEL TEST 12.1 FAILED");
            return false;
        }

        res = badModel.CAPMemberSignOut(member1.capId, "TIME");
        if (res != ModelState.ERROR_DB_COM_FAILED) {
            System.out.println("MODEL TEST 13.1 FAILED");
            return false;
        }

        res = badModel.guestSignIn(guest1, "TIME");
        if (res != ModelState.ERROR_DB_COM_FAILED) {
            System.out.println("MODEL TEST 14.1 FAILED");
            return false;
        }

        res = badModel.guestSignOut(guest1, "TIME");
        if (res != ModelState.ERROR_DB_COM_FAILED) {
            System.out.println("MODEL TEST 15.1 FAILED");
            return false;
        }

        res = badModel.setMissionStart("New time");
        if (res != ModelState.ERROR_DB_COM_FAILED) {
            System.out.println("MODEL TEST 16.1 FAILED");
            return false;
        }

        res = badModel.setMissionEnd("New time");
        if (res != ModelState.ERROR_DB_COM_FAILED) {
            System.out.println("MODEL TEST 17.1 FAILED");
            return false;
        }

        return true;
    }

    private boolean runGeneralTests() {
        ModelState res;

        String missionName = "UnitTesting";
        Mission m;

        CAPMember member1 = new CAPMember();
        member1.capId = "123456";
        member1.qualifications = "none";
        member1.name = "Bob Smith";
        member1.ecName = "Alice Jones";
        member1.ecPhone = "(111)222-3334";

        Guest guest1 = new Guest();
        guest1.name = "John Johnson";
        guest1.ecName = "Alice Jones";
        guest1.ecPhone = "1112223334";

        //List<Mission> missionList = new ArrayList<Mission>();

        // Verify connection
        res = model.checkConnection();
        if (res != ModelState.SUCCESS) {
            System.out.println("MODEL TEST 1.2 FAILED");
            return false;
        }

        // No mission in progress yet
        res = model.isMissionInProgress();
        if (res != ModelState.NOT_IN_PROGRESS) {
            System.out.println("MODEL TEST 2.2 FAILED");
            return false;
        }

        // Start a mission
        res = model.startMission(missionName);
        if (res != ModelState.SUCCESS) {
            System.out.println("MODEL TEST 3.2 FAILED");
            return false;
        }

        // Verify mission started
        res = model.isMissionInProgress();
        if (res != ModelState.IN_PROGRESS) {
            System.out.println("MODEL TEST 2.3 FAILED");
            return false;
        }

        // Can't start a mission if one is already in progress
        res = model.startMission(missionName);
        if (res != ModelState.ERROR_ALREADY_IN_PROGRESS) {
            System.out.println("MODEL TEST 3.3 FAILED");
            return false;
        }

        // Editing a member who is not in the database should fail
        res = model.editCAPInfo(member1);
        if (res == ModelState.SUCCESS) {
            System.out.println("MODEL TEST 10.2 FAILED");
            return false;
        }

        // Create CAP account
        res = model.addCAPMember(member1);
        if (res != ModelState.SUCCESS) {
            System.out.println("MODEL TEST 9.2 FAILED");
            return false;
        }

        // Edit CAP info 
        member1.ecName = "Alice Smith";
        res = model.editCAPInfo(member1);
        if (res != ModelState.SUCCESS) {
            System.out.println("MODEL TEST 10.3 FAILED");
            return false;
        }

        // Get CAP member to verify edit
        GetCAPMemberRet resCAP = model.getCAPMember("123456");
        if (resCAP.state != ModelState.SUCCESS || resCAP.member == null ||
            !(resCAP.member.capId.equals(member1.capId) && resCAP.member.name.equals(member1.name) && 
            resCAP.member.ecName.equals(member1.ecName) && resCAP.member.ecPhone.equals(member1.ecPhone) &&
            resCAP.member.qualifications.equals(member1.qualifications))) {
            System.out.println("MODEL TEST 11.2 FAILED");
            return false;
        }

        // Member sign in
        res = model.CAPMemberSignIn(member1.capId, "in 1");
        if (res != ModelState.SUCCESS) {
            System.out.println("MODEL TEST 12.2 FAILED");
            return false;
        }

        // Member sign out
        res = model.CAPMemberSignOut(member1.capId, "out 1");
        if (res != ModelState.SUCCESS) {
            System.out.println("MODEL TEST 13.2 FAILED");
            return false;
        }

        // Guest sign in
        res = model.guestSignIn(guest1, "in 2");
        if (res != ModelState.SUCCESS) {
            System.out.println("MODEL TEST 14.2 FAILED");
            return false;
        }

        // Guest sign out
        res = model.guestSignOut(guest1, "out 2");
        if (res != ModelState.SUCCESS) {
            System.out.println("MODEL TEST 15.2 FAILED");
            return false;
        }

        // Member sign in again
        res = model.CAPMemberSignIn(member1.capId, "in 3");
        if (res != ModelState.SUCCESS) {
            System.out.println("MODEL TEST 12.3 FAILED");
            return false;
        }

        // Member sign out again
        res = model.CAPMemberSignOut(member1.capId, "out 3");
        if (res != ModelState.SUCCESS) {
            System.out.println("MODEL TEST 13.3 FAILED");
            return false;
        }

        // Set mission start time
        String missionStart = "Start time";
        res = model.setMissionStart(missionStart);
        if (res != ModelState.SUCCESS) {
            System.out.println("MODEL TEST 16.2 FAILED");
            return false;
        }

        // Set mission end time
        String missionEnd = "End time";
        res = model.setMissionEnd(missionEnd);
        if (res != ModelState.SUCCESS) {
            System.out.println("MODEL TEST 17.2 FAILED");
            return false;
        }

        // End the mission
        res = model.endMission();
        if (res != ModelState.SUCCESS) {
            System.out.println("MODEL TEST 4.2 FAILED");
            return false;
        }

        // Verify there is exactly one mission available and that the name is correct
        GetAllMissionsRet allMissions = model.getAllMissions();
        List<Mission> missionList = allMissions.missions;
        if (allMissions.state != ModelState.SUCCESS || missionList == null || missionList.size() != 1) {
            System.out.println("MODEL TEST 5.2.1 FAILED");
            return false;
        } else {
            m = missionList.get(0);
            if (!m.incidentName.equals(missionName)) {
                System.out.println("MODEL TEST 5.2.2 FAILED");
                return false;
            }
        }

        // Get and verify all mission attendees
        GetAttendeesRet resAttendees = model.getAttendees(m);
        List<Person> attendees = resAttendees.attendees;
        if (resAttendees.state != ModelState.SUCCESS || attendees == null || attendees.size() != 3) {
            System.out.println("MODEL TEST 8.2.1  FAILED");
            return false;
        } else {
            for (int i = 0; i < attendees.size(); i++) {
                Person p = attendees.get(i);
                if (p instanceof CAPMember && !(p.name.equals(member1.name) && 
                    p.ecName.equals(member1.ecName) && p.ecPhone.equals(member1.ecPhone) &&
                    ((CAPMember)p).capId.equals(member1.capId) && ((CAPMember)p).qualifications.equals(member1.qualifications))) {
                    System.out.println("MODEL TEST 8.2.2  FAILED");
                    return false;
                } else if (p instanceof Guest && !(p.name.equals(guest1.name) && 
                    p.ecName.equals(guest1.ecName) && p.ecPhone.equals(guest1.ecPhone))) {
                    System.out.println("MODEL TEST 8.2.3  FAILED");
                    return false;
                }
            }
        }

        // Generate form 211 for the mission
        res = model.generateForm211(m);
        if (res != ModelState.SUCCESS) {
            System.out.println("MODEL TEST 7.2 FAILED");
            //return false;
        } else {
            System.out.println("Generated form 211. Check for correct output");
        }

        // Delete the mission
        res = model.deleteMission(m);
        if (res != ModelState.SUCCESS) {
            System.out.println("MODEL TEST 6.2 FAILED");
            return false;
        }

        // final clean up
        model.dbct.removeMember(member1.capId);

        return true;
    }
}