/* Feedback.java
 *
 * Interface for barcode sign in station user feedback
 *
 */

public interface Feedback {
    public void comFailed();
    public void shutDown();
    public void startupSuccessful();
    public void badCAPId();
    public void notInDatabase();
    public void signInSuccessful();
    public void signOutSuccessful();
}
