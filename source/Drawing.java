import java.awt.Color;
import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.BasicStroke;

import java.awt.*;
import java.awt.event.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import javax.swing.*;

//this class has all of the lines in here to create the outline of a 211 form (but only for the top section down to 4. Name  etc then the rest will be handled in another class as it will
//be repeatable for when more lines get added in and will be called multiple times
public class Drawing extends JPanel {
        Graphics go;
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            
            
           
            //drawlines go like this (x1,y1,x2,y2)
            
            
            Graphics2D g2d = (Graphics2D) g;
            go = g2d;
            //sets stroke aka the line thickness
            g2d.setStroke(new BasicStroke(2));
            //main outline from top to under name
            g2d.drawLine(10, 10, 1480, 10);
            g2d.drawLine(10,10,10,240);
            g2d.drawLine(1480,10,1480,240);
            g2d.drawLine(10,240,1480,240);
            
            //horizontal lines from left to right for name and personal info
            g2d.drawLine(10,200,1320,200);
            g2d.drawLine(10,160,1480,160);
            
            //line going down from initial incident check in to bottom line under name
            g2d.drawLine(1320,160,1320,240);
            
            //line drawn in between In and Out in time box
            g2d.drawLine(1380,200,1380,240);
            
            //line drawn to the left of sign in time
            g2d.drawLine(1280,240,1280,2780);
            
            //drawing line between in and out
            g2d.drawLine(1380,240,1380,2780);
            
            //lines in between incident name, operation period, check-in location, checkin list
            g2d.drawLine(375, 10, 375,160);
            g2d.drawLine(830, 10, 830,160);
            g2d.drawLine(1270, 10, 1270,160);
            
            
            
            //lines inbetween name, capid, ics section, emergency contact and the (x)
            g2d.drawLine(370, 200, 370,240);
            g2d.drawLine(615, 200, 615,240);
            g2d.drawLine(940, 200, 940,240);
            g2d.drawLine(1280, 200, 1280,240);
            
            
            //line from name to bottom of form
            g2d.drawLine(370,240,370,2780);
            
            //line from ics to bottom of form
            g2d.drawLine(615,240,615,2780);
            
            //other 2 lines from middle of form to bottom
            g2d.drawLine(940, 240, 940,2780);
            g2d.drawLine(1280, 240, 1280,2780);
            
            //lines coming down from top box to bottom of form
            g2d.drawLine(10,240, 10,2780);
            g2d.drawLine(1480,240, 1480,2780);
            
            //line on bottom of form
            g2d.drawLine(10,2780,1480,2780);
            
            //lines in between each text field
            g2d.drawLine(10,307,1480,307);
            g2d.drawLine(10,362,1480,362);
            // +57
            g2d.drawLine(10,419,1480,419);
            // +57
            g2d.drawLine(10,475,1480,475);
            
            for(int x=0;x<41;x++){
               g2d.drawLine(10,475 + (56*x),1480,475 + (56*x) );
            }
            
            drawTextFieldOutline(240);
            
        }
        
        //this function can be called to generate 8 more lines in the 211 form for when we want to add 8 more text field lines as the users have almost filled up
        //all the existing ones
        //it takes in an it that is the starting location for how far down on the panel it should begin drawing (this is going to be the same value as the ending y location of the previous last drawn box for nname/ capid /ics etc)
        protected void drawTextFieldOutline(int y){
            go.drawLine(1280, 200, 1280,240);
        }
    }
