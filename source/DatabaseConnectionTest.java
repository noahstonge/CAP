/* DatabaseConnectionTest.java
 *
 * Unit tests for DatabaseConnection.java
 * These tests are meant to be run on a database which has already
 * been initialized, but is currently empty.
 *
 * Returns database to it's empty state upon successfull completion
 *
 * Ryan Erdahl
 * 10/09/2021 
 */
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DatabaseConnectionTest {
    DatabaseConnection db;

    public DatabaseConnectionTest(String url, String user, String password) {
        db = new DatabaseConnection(url, user, password);
        db.printStackTrace = true;
    }

    public boolean runAll() {
        AttendeeStatus status;
        CAPMember member   = new CAPMember();
        member.capId       = "123456";
        member.name        = "John Doe";
        member.qualifications = "None";
        member.ecName      = "Jill Buck";
        member.ecPhone     = "1111111111";

        Guest guest = new Guest();
        guest.name        = "Bob Smith";
        guest.ecName      = "Alice Johnson";
        guest.ecPhone     = "(333)333-3333";

        // Check ability to connect with db
        if (!db.checkConnection()) {
            System.out.println("DATABASE CONNECTION TEST 1 FAILED");
            return false;
        }

        // Verify there is not a mission in progress
        Boolean mis = db.missionInProgress();
        if (mis == null || mis == true) {
            System.out.println("DATABASE CONNECTION TEST 2 FAILED");
            return false;
        }

        // Check mission id starts at 0
        if (db.curMissionId() != 0) {
            System.out.println("DATABASE CONNECTION TEST 3 FAILED");
            return false;
        }

        // Start the mission
        if (!db.startMission("Test Mission")) {
            System.out.println("DATABASE CONNECTION TEST 4 FAILED");
            return false;
        }

        // Verify mission started
        mis = db.missionInProgress();
        if (mis == null || mis == false) {
            System.out.println("DATABASE CONNECTION TEST 5 FAILED");
            return false;
        }

        // Verify mission id incremented
        if (db.curMissionId() != 1) {
            System.out.println("DATABASE CONNECTION TEST 6 FAILED");
            return false;
        }

        // Initial member status
        status = db.getMemberStatus(member.capId);
        if (status.inDatabase == true || status.signedIn == true) {
            System.out.println("DATABASE CONNECTION TEST 7 FAILED");
            return false;
        }

        // Add valid member
        if (!db.addMember(member)) {
            System.out.println("DATABASE CONNECTION TEST 8 FAILED");
            return false;
        }

        // Check get member for both a valid member and an invalid member
        CAPMember res = db.getMember("111111");
        if (res != null) {
            System.out.println("DATABASE CONNECTION TEST 9 FAILED");
            return false;
        }

        res = db.getMember(member.capId);
        if (res == null || !(res.capId.equals(member.capId) && res.name.equals(member.name) &&
            res.ecName.equals(member.ecName) && res.ecPhone.equals(member.ecPhone))) {
            System.out.println("DATABASE CONNECTION TEST 10 FAILED");
            return false;
        }

        // Get member status
        status = db.getMemberStatus(member.capId);
        if (status.inDatabase == false || status.signedIn == true) {
            System.out.println("DATABASE CONNECTION TEST 11 FAILED");
            return false;
        }

        // Sign-in
        if (!db.memberSignIn(member.capId, "18:00")) {
            System.out.println("DATABASE CONNECTION TEST 12 FAILED");
            return false;
        }

        // Get member status
        status = db.getMemberStatus(member.capId);
        if (status.inDatabase == false || status.signedIn == false) {
            System.out.println("DATABASE CONNECTION TEST 13 FAILED");
            return false;
        }

        // Sign-out
        if (!db.memberSignOut(member.capId, "18:30")) {
            System.out.println("DATABASE CONNECTION TEST 14 FAILED");
            return false;
        }

        // Get member status
        status = db.getMemberStatus(member.capId);
        if (status.inDatabase == false || status.signedIn == true) {
            System.out.println("DATABASE CONNECTION TEST 15 FAILED");
            return false;
        }

        // Sign in member a second time
        if (!db.memberSignIn(member.capId, "19:00")) {
            System.out.println("DATABASE CONNECTION TEST 16 FAILED");
            return false;
        }

        // Get member status
        status = db.getMemberStatus(member.capId);
        if (status.inDatabase == false || status.signedIn == false) {
            System.out.println("DATABASE CONNECTION TEST 17 FAILED");
            return false;
        }

        // Attempt to sign in when already signed in
        if (db.memberSignIn(member.capId, "19:01")) {
            System.out.println("DATABASE CONNECTION TEST 18 FAILED");
            return false;
        }

        // Get member status
        status = db.getMemberStatus(member.capId);
        if (status.inDatabase == false || status.signedIn == false) {
            System.out.println("DATABASE CONNECTION TEST 19 FAILED");
            return false;
        }

        // Sign-out the second time
        if (!db.memberSignOut(member.capId, "20:00")) {
            System.out.println("DATABASE CONNECTION TEST 20 FAILED");
            return false;
        }

        // Get members from today's mission
        ArrayList<CAPMember> attends = db.getMissionMembers(1);
        if (attends == null || attends.size() != 2) {
            System.out.println("DATABASE CONNECTION TEST 21 FAILED");
            return false;
        }

        // Verify member is correct
        res = attends.get(0);
        if (!(res.capId.equals(member.capId) && res.name.equals(member.name) &&
            res.ecName.equals(member.ecName) && res.ecPhone.equals(member.ecPhone))) {
            System.out.println("DATABASE CONNECTION TEST 22 FAILED");
            return false;
        }

        // Edit all fields
        member.name        = "John X Doe";
        member.qualifications = "All of them";
        member.ecName      = "Jill X Buck";
        member.ecPhone     = "2222222222";
        if (!db.editMember(member)) {
            System.out.println("DATABASE CONNECTION TEST 23 FAILED");
            return false;
        }

        // Check edit
        attends = db.getMissionMembers(1);
        if (attends == null || attends.size() != 2) {
            System.out.println("DATABASE CONNECTION TEST 24 FAILED");
            return false;
        }

        // Verify member is correct
        res = attends.get(0);
        if (!(res.capId.equals(member.capId) && res.name.equals(member.name) &&
            res.ecName.equals(member.ecName) && res.ecPhone.equals(member.ecPhone))) {
            System.out.println("DATABASE CONNECTION TEST 25 FAILED");
            return false;
        }

        // Initial guest status
        status = db.guestStatus(guest.name);
        if (status.inDatabase == true || status.signedIn == true) {
            System.out.println("DATABASE CONNECTION TEST 26 FAILED");
            return false;
        }

        // Add guest
        if (!db.addGuest(guest, "19:02")) {
            System.out.println("DATABASE CONNECTION TEST 27 FAILED");
            return false;
        }

        // Check that guest exists
        status = db.guestStatus(guest.name);
        if (status.inDatabase == false || status.signedIn == false) {
            System.out.println("DATABASE CONNECTION TEST 28 FAILED");
            return false;
        }

        // Sign out guest
        if (!db.guestSignOut(guest.name, "19:58")) {
            System.out.println("DATABASE CONNECTION TEST 29 FAILED");
            return false;
        }

        // Verify guest sign out
        status = db.guestStatus(guest.name);
        if (status.inDatabase == false || status.signedIn == true) {
            System.out.println("DATABASE CONNECTION TEST 30 FAILED");
            return false;
        }

        // Update mission start time
        String startDateTime = "1 January 2000 / 17:00";
        if (!db.updateMissionStart(startDateTime)) {
            System.out.println("DATABASE CONNECTION TEST 31 FAILED");
            return false;
        }

        // Update mission end time
        String endDateTime = "1 January 2000 / 21:00";
        if (!db.updateMissionEnd(endDateTime)) {
            System.out.println("DATABASE CONNECTION TEST 32 FAILED");
            return false;
        }

        // End the mission
        if (!db.endMission()) {
            System.out.println("DATABASE CONNECTION TEST 33 FAILED");
            return false;
        }

        // Get mission list
        ArrayList<Mission> missions = db.getAllMissions();
        if (missions == null || missions.size() != 1) {
            System.out.println("DATABASE CONNECTION TEST 34 FAILED");
            return false;
        }

        // Verify mission correct
        Mission m = missions.get(0);
        if (!(m.incidentName.equals("Test Mission") && m.missionId == 1 &&
            m.startDateTime.equals(startDateTime) && m.endDateTime.equals(endDateTime) && m.inProgress == false)) {
            System.out.println("DATABASE CONNECTION TEST 35 FAILED");
            return false;
        }

        // End the mission
        if (!db.endMission()) {
            System.out.println("DATABASE CONNECTION TEST 36 FAILED");
            return false;
        }

        // Get members on mission
        attends = db.getMissionMembers(1);
        if (attends == null || attends.size() != 2) {
            System.out.println("DATABASE CONNECTION TEST 37 FAILED");
            return false;
        }

        // Verify member is correct
        res = attends.get(0);
        if (!(res.capId.equals(member.capId) && res.name.equals(member.name) &&
            res.ecName.equals(member.ecName) && res.ecPhone.equals(member.ecPhone))) {
            System.out.println("DATABASE CONNECTION TEST 38 FAILED");
            return false;
        }

        // Get guests on mission
        ArrayList<Guest> attendsG = db.getMissionGuests(1);
        if (attendsG == null || attendsG.size() != 1) {
            System.out.println("DATABASE CONNECTION TEST 39 FAILED");
            return false;
        }

        // Verify guest is correct
        Guest resG = attendsG.get(0);
        if (!(resG.name.equals(guest.name) && resG.ecName.equals(guest.ecName) && 
            resG.ecPhone.equals(guest.ecPhone))) {
            System.out.println("DATABASE CONNECTION TEST 40 FAILED");
            return false;
        }

        // Delete all guests and members from mission date
        if (!db.removeMission(1)) {
            System.out.println("DATABASE CONNECTION TEST 41 FAILED");
            return false;
        }

        // Get stuff from mission and verify no one is there
        attends = db.getMissionMembers(1);
        if (attends == null || attends.size() != 0) {
            System.out.println("DATABASE CONNECTION TEST 42 FAILED");
            return false;
        }

        attendsG = db.getMissionGuests(1);
        if (attendsG == null || attendsG.size() != 0) {
            System.out.println("DATABASE CONNECTION TEST 43 FAILED");
            return false;
        }

        // Remove member from database
        if (!db.removeMember(member.capId)) {
            System.out.println("DATABASE CONNECTION TEST 44 FAILED");
            return false;
        }

        // Check member status
        status = db.getMemberStatus(member.capId);
        if (status.inDatabase == true || status.signedIn == true) {
            System.out.println("DATABASE CONNECTION TEST 45 FAILED");
            return false;
        }

        return true;
    }
}
