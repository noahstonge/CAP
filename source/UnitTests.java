/* UnitTests.java
 *
 * Unit tests for DatabaseConnection.java
 *
 */

public class UnitTests {
    public static void main(String[] args) {
        // Test database network connection API
        DatabaseConnectionTest dbct = new DatabaseConnectionTest("jdbc:mysql://localhost/cap_db", "test", "testing");
        System.out.println("Running database connection tests...");
        if(dbct.runAll()) {
            System.out.println("Success!");
        } else {
            System.out.println("FAILURE");
        }

        // Test Model
        ModelTest modelT = new ModelTest("jdbc:mysql://localhost/cap_db", "test", "testing");
        System.out.println("Running model tests...");
        if(modelT.runAll()) {
            System.out.println("Success!");
        } else {
            System.out.println("FAILURE");
        }
    }
}