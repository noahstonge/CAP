/* BarcodeSignIn.java
 *
 * Class and main program to run Barcode Sign Ins
 * Communicates directly with the database over the network connection
 *
 */

import java.util.Scanner;

public class BarcodeSignIn {
    public static DatabaseConnection getDatabaseConnection() {
        String url = "jdbc:mysql://localhost/cap_db";
        String user = "test";
        String password = "testing";

        return new DatabaseConnection(url, user, password);
    }

    public static boolean validateCAPId(String id) {
        int capIDAsInt;

        // Convert CAP ID to integer
        try {
            capIDAsInt = Integer.parseInt(id);
        } catch (NumberFormatException e) {
            return false;
        }

        // Must be 6 digits
        if (id.length() != 6) {
            return false;
        }

        return true;
    }

    public static void main(String[] args) {
        // Database location and credentials
        DatabaseConnection dbct = getDatabaseConnection();

        // Setup user feedback
        Feedback fb = new TextFeedback();

        // Verify connection with database
        if(!dbct.checkConnection()) {
            fb.comFailed();
            fb.shutDown();
            System.exit(-1);
        }

        // Barcode gives input as a keyboard to the console
        Scanner in = new Scanner(System.in);

        // Notify that startup succeeded
        fb.startupSuccessful();

        // Main program loop
        while (true) {
            // Get CAP ID and make sure it is valid
            String id = in.nextLine();
            if (!validateCAPId(id)) {
                fb.badCAPId();
                continue;
            }

            // Check if member is in database and whether they are signed in
            AttendeeStatus status = dbct.getMemberStatus(id);

            // Check if comm failed
            if (status == null) {
                fb.comFailed();
                continue;
            }

            // Verify that CAP ID is in database
            if (status.inDatabase == false) {
                fb.notInDatabase();
                continue;
            }

            // Sign in or sign out
            if (status.signedIn == false) {
                if (dbct.memberSignIn(id)) {
                    fb.signInSuccessful();
                } else {
                    fb.comFailed();
                }
            } else {
                if (dbct.memberSignOut(id)) {
                    fb.signOutSuccessful();
                } else {
                    fb.comFailed();
                }
            }

        }
    }
}