/* DatabaseConnection.java
 *
 * Class to connect to and communicate with the database over
 * a network connection.
 *
 * Ryan Erdahl
 * 10/09/2021 
 */

import java.sql.*;
import java.time.LocalTime;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class DatabaseConnection {
	String url;
    String user;
    String password;
    boolean printStackTrace = false;

	public DatabaseConnection(String url, String user, String password) {
        // Needed for encryption and client/server authentication
        url +=  "?verifyServerCertificate=true" +
               "&useSSL=true" +
               "&requireSSL=true";
        System.setProperty("javax.net.ssl.trustStore","../certs/truststore"); 
        System.setProperty("javax.net.ssl.trustStorePassword","password");
        System.setProperty("javax.net.ssl.keyStore","../certs/keystore"); 
        System.setProperty("javax.net.ssl.keyStorePassword","password");

		this.url = url;
        this.user = user;
        this.password = password;
	}

    // Checks that the database located at url can be accessed using
    // the username and password for this instance.
    // Should be called after initialization to verify this.
    public boolean checkConnection() {
        boolean ret = false;

        try {
            Connection conn = DriverManager.getConnection(url, user, password);
            ret = true;
        } catch (SQLException e) {
            if (printStackTrace) e.printStackTrace();
        }

        return ret;
    }

    // Adds a new CAP member to the database
    // Assumes that all fields for the member parameter are filled
    // Returns true on success, false otherwise
    public boolean addMember(CAPMember member) {
        boolean ret = false;
        String sql = "INSERT INTO CAP_member VALUES (?, ?, ?, ?, ?)";

        // Send to database
        try (Connection conn = DriverManager.getConnection(url, user, password);) {
            try (PreparedStatement pst = conn.prepareStatement(sql)) {
                pst.setString(1, member.capId);
                pst.setString(2, member.name);
                pst.setString(3, member.qualifications);
                pst.setString(4, member.ecName);
                pst.setString(5, member.ecPhone);
                int rs = pst.executeUpdate();

                // No exception, success
                if (rs == 1) ret = true;
            }
        } catch (SQLException e) {
            if (printStackTrace) e.printStackTrace();
        }

        return ret;
    }

    // Updates info for a given CAP member
    // Assumes that the CAP member is in the database already
    // Does not update fields which are left as null in the member parameter
    // Returns true on success, false on failure
    public boolean editMember(CAPMember member) {
        boolean ret = false;
        String sql = "UPDATE CAP_member SET name=?, qualifications=?, ec_name=?, ec_phonenum=? WHERE CAP_ID=?";
        
        // Send to database
        try (Connection conn = DriverManager.getConnection(url, user, password);) {
            try (PreparedStatement pst = conn.prepareStatement(sql)) {
                pst.setString(1, member.name);
                pst.setString(2, member.qualifications);
                pst.setString(3, member.ecName);
                pst.setString(4, member.ecPhone);
                pst.setString(5, member.capId);
                int rs = pst.executeUpdate();

                // No exception, success
                if (rs == 1) ret = true;
            }
        } catch (SQLException e) {
            if (printStackTrace) e.printStackTrace();
        }

        return ret;
    }

    // Removes a CAP member with the given CAP ID from the database
    // Returns true on success, returns false if capId is not in the database, or on failure
    public boolean removeMember(String capId) {
        boolean ret = false;
        String sql = "DELETE FROM CAP_member WHERE CAP_ID=?";

        try (Connection conn = DriverManager.getConnection(url, user, password);) {
            try (PreparedStatement pst = conn.prepareStatement(sql)) {
                pst.setString(1, capId);
                int rs = pst.executeUpdate();

                // No exceptions, check for success
                if (rs > 0) ret = true;
            }
        } catch (SQLException e) {
            if (printStackTrace) e.printStackTrace();
        }

        return ret;
    }

    // Checks if a CAP member is already in the database
    // and whether they are currently signed in
    // Returns null on error
    public AttendeeStatus getMemberStatus(String capId) {
        AttendeeStatus ret = null;
        AttendeeStatus temp = new AttendeeStatus();
        ResultSet rs;
        String sql = "SELECT CAP_ID FROM CAP_member where CAP_ID=?";
        int missionId = this.curMissionId();

        try (Connection conn = DriverManager.getConnection(url, user, password);) {
            // Check if capId is in the database
            try (PreparedStatement pst = conn.prepareStatement(sql)) {
                pst.setString(1, capId);
                rs = pst.executeQuery();

                if (rs.isBeforeFirst()) {
                    temp.inDatabase = true;
                } else {
                    temp.inDatabase = false;
                }
            }

            // Check if currently signed in
            sql = "SELECT signout_time FROM member_attendee WHERE CAP_ID=? and mission_id=?";
            try (PreparedStatement pst = conn.prepareStatement(sql)) {
                pst.setString(1, capId);
                pst.setInt(2, missionId);
                rs = pst.executeQuery();

                // Assume not signed in then look for an entry with a null sign out time
                temp.signedIn = false;
                while (rs.next()) {
                    String signOut = rs.getString("signout_time");
                    if (signOut == null) {
                        temp.signedIn = true;
                    }
                }

                // No exceptions, return temp
                ret = temp;
            }
        } catch (SQLException e) {
            if (printStackTrace) e.printStackTrace();
        }

        return ret;
    }

    /**
     * Gets a CAPMember identified by their CAP ID from the database
     * @param id the CAP ID of the member to be fetched
     * @return the CAPMember on success or null on failure
     */
    public CAPMember getMember(String id) {
        String sql;
        CAPMember ret = null;

        AttendeeStatus status = this.getMemberStatus(id);
        if (status == null) {
            return null;
        }

        try (Connection conn = DriverManager.getConnection(url, user, password);) {
            if (status.inDatabase) {
                sql = "SELECT * FROM CAP_member WHERE CAP_ID=?";
                try (PreparedStatement pst = conn.prepareStatement(sql)) {
                    pst.setString(1, id);
                    ResultSet rst = pst.executeQuery();

                    CAPMember member = new CAPMember();
                    if (rst.next()) {
                        member.capId       = rst.getString("CAP_ID");
                        member.name        = rst.getString("name");
                        member.ecName      = rst.getString("ec_name");
                        member.ecPhone     = rst.getString("ec_phonenum");
                        member.qualifications = rst.getString("qualifications");

                        // Successfully retrieved member information
                        ret = member;
                    }
                }
            }
        } catch (SQLException e) {
            if (printStackTrace) e.printStackTrace();
        }

        return ret;             
    }

    // Signs in a CAP member with the given CAP ID with the given sign in time
    // Assumes that the member is already in the database
    // Returns true on success, false on failure
    public boolean memberSignIn(String capId, String inTime) {
        boolean ret = false;
        String sql = "INSERT INTO member_attendee (CAP_ID, mission_id, signin_time) VALUES(?,?,?)";

        // Get the mission number to use
        int missionId = curMissionId();
        if (missionId < 0) {
            return false;
        }

        // Only sign in if not currently signed in
        AttendeeStatus status = getMemberStatus(capId);
        if (status == null || status.signedIn) {
            return false;
        }
        
        try (Connection conn = DriverManager.getConnection(url, user, password);) {
            try (PreparedStatement pst = conn.prepareStatement(sql)) {
                pst.setString(1, capId);
                pst.setInt(2, missionId);
                pst.setString(3, inTime);
                int rs = pst.executeUpdate();

                // No exception, check for success
                if (rs == 1) ret = true;
            }
        } catch (SQLException e) {
            if (printStackTrace) e.printStackTrace();
        }

        return ret;
    }

    // Signs out a CAP member with the given CAP ID with the given sign out time
    // Assumes that the member is already signed in
    // Returns true on success, false on failure
    public boolean memberSignOut(String capId, String outTime) {
        boolean ret = false;
        String sql = "UPDATE member_attendee SET signout_time=? WHERE CAP_ID=? and mission_id=? and signout_time IS NULL;";

        // Get the mission number to use
        int missionId = curMissionId();
        if (missionId < 0) {
            return false;
        }

        // Only sign in if currently signed in
        AttendeeStatus status = getMemberStatus(capId);
        if (status == null || !status.signedIn) {
            return false;
        }
        
        try (Connection conn = DriverManager.getConnection(url, user, password);) {
            try (PreparedStatement pst = conn.prepareStatement(sql)) {
                pst.setString(1, outTime);
                pst.setString(2, capId);
                pst.setInt(3, missionId);
                int rs = pst.executeUpdate();

                // No exception, check for success
                if (rs == 1) ret = true;
            }
        } catch (SQLException e) {
            if (printStackTrace) e.printStackTrace();
        }

        return ret;
    }

    // Adds a guest to the database and signs them in with the given sign in time
    // Assumes a guest with this first and last name is not
    // already in the database for today
    // Returns true on success, false on failure
    public boolean addGuest(Guest guest, String inTime) {
        boolean ret = false;
        String sql = "INSERT INTO guest_attendee (name, ec_name, ec_phonenum, mission_id, signin_time) VALUES (?,?,?,?,?)";

        // Get the mission number to use
        int missionId = curMissionId();
        if (missionId < 0) {
            return false;
        }

        try (Connection conn = DriverManager.getConnection(url, user, password);) {
            try (PreparedStatement pst = conn.prepareStatement(sql)) {
                pst.setString(1, guest.name);
                pst.setString(2, guest.ecName);
                pst.setString(3, guest.ecPhone);
                pst.setInt(4, missionId);
                pst.setString(5, inTime);
                int rs = pst.executeUpdate();

                // No exception, check for success
                if (rs == 1) ret = true;
            }
        } catch (SQLException e) {
            if (printStackTrace) e.printStackTrace();
        }

        return ret;
    }

    // Checks if a guest with the given name is in the database for today
    // and whether or not they are currently signed in
    // Returns NULL on error
    public AttendeeStatus guestStatus(String name) {
        AttendeeStatus ret = null;
        AttendeeStatus temp = new AttendeeStatus();
        ResultSet rs;
        String sql = "SELECT signout_time FROM guest_attendee WHERE name=? and mission_id=?";

        // Get the mission number to use
        int missionId = curMissionId();
        if (missionId < 0) {
            return null;
        }

        try (Connection conn = DriverManager.getConnection(url, user, password);) {
            try (PreparedStatement pst = conn.prepareStatement(sql)) {
                pst.setString(1, name);
                pst.setInt(2, missionId);
                rs = pst.executeQuery();

                // Assume not signed in or in database then look for an entry with a null sign out time
                temp.inDatabase = false;
                temp.signedIn = false;
                if (rs.next()) {
                    temp.inDatabase = true;
                    rs.getString("signout_time");
                    if (rs.wasNull()) {
                        temp.signedIn = true;
                    }
                }

                // No exceptions, return temp
                ret = temp;
            }
        } catch (SQLException e) {
            if (printStackTrace) e.printStackTrace();
        }

        return ret;
    }

    // Signs out the specified guest
    // Returns true on success, false on failure
    public boolean guestSignOut(String name, String outTime) {
        boolean ret = false;
        String sql = "UPDATE guest_attendee SET signout_time=? WHERE name=? and mission_id=?";

        // Get the mission number to use
        int missionId = curMissionId();
        if (missionId < 0) {
            return false;
        }

        try (Connection conn = DriverManager.getConnection(url, user, password);) {
            try (PreparedStatement pst = conn.prepareStatement(sql)) {
                pst.setString(1, outTime);
                pst.setString(2, name);
                pst.setInt(3, missionId);
                int rs = pst.executeUpdate();

                // No exception, check for success
                if (rs == 1) ret = true;
            }
        } catch (SQLException e) {
            if (printStackTrace) e.printStackTrace();
        }

        return ret;
    }

    // Gets a list of all CAP members who were a part of a mission with a given mission id
    // Returns NULL on failure
    public ArrayList<CAPMember> getMissionMembers(int missionId) {
        ArrayList<CAPMember> ret = null;
        ArrayList<CAPMember> temp = new ArrayList<CAPMember>();
        String sql = "SELECT * FROM CAP_member NATURAL JOIN member_attendee WHERE mission_id=?";

        try (Connection conn = DriverManager.getConnection(url, user, password);) {
            try (PreparedStatement pst = conn.prepareStatement(sql)) {
                pst.setInt(1, missionId);
                ResultSet rst = pst.executeQuery();

                // Fill list with CAPMembers
                while (rst.next()) {
                    CAPMember member = new CAPMember();
                    member.capId       = rst.getString("CAP_ID");
                    member.name        = rst.getString("name");
                    member.ecName      = rst.getString("ec_name");
                    member.ecPhone     = rst.getString("ec_phonenum");
                    member.qualifications = rst.getString("qualifications");
                    member.signIn      = rst.getString("signin_time");
                    member.signOut     = rst.getString("signout_time");
                    temp.add(member);
                }

                // No exception, return temp
                ret = temp;
            } 
        } catch (SQLException e) {
            if (printStackTrace) e.printStackTrace();
        }

        return ret;
    }

    // Gets a list of all guests who were a part of a mission with a given mission id
    // Returns NULL on failure
    public ArrayList<Guest> getMissionGuests(int missionId) {
        ArrayList<Guest> ret = null;
        ArrayList<Guest> temp = new ArrayList<Guest>();
        String sql = "SELECT * FROM guest_attendee WHERE mission_id=?";

        try (Connection conn = DriverManager.getConnection(url, user, password);) {
            try (PreparedStatement pst = conn.prepareStatement(sql)) {
                pst.setInt(1, missionId);
                ResultSet rst = pst.executeQuery();

                // Fill list with Guests
                while (rst.next()) {
                    Guest guest = new Guest();
                    guest.name        = rst.getString("name");
                    guest.ecName      = rst.getString("ec_name");
                    guest.ecPhone     = rst.getString("ec_phonenum");
                    guest.signIn      = rst.getString("signin_time");
                    guest.signOut     = rst.getString("signout_time");
                    guest.missionId = missionId;
                    temp.add(guest);
                }

                // No exception, return temp
                ret = temp;
            } 
        } catch (SQLException e) {
            if (printStackTrace) e.printStackTrace();
        }

        return ret;
    }

    // Removes the mission and all guests and member attendee entries for
    // the mission with the given mission id from the database
    // Returns true on success, false on failure
    public boolean removeMission(int missionId) {
        boolean ret = false;
        String sql = "DELETE FROM member_attendee WHERE mission_id=?";

        try (Connection conn = DriverManager.getConnection(url, user, password);) {
            try (PreparedStatement pst = conn.prepareStatement(sql)) {
                pst.setInt(1, missionId);
                pst.executeUpdate();

                // No exceptions, success for this command
                ret = true;
            }

            sql = "DELETE FROM guest_attendee WHERE mission_id=?";
            try (PreparedStatement pst = conn.prepareStatement(sql)) {
                pst.setInt(1, missionId);
                pst.executeUpdate();

                // No exceptions, success only if previous query was successful
                ret &= true;
            }

            sql = "DELETE FROM mission WHERE mission_id=?";
            try (PreparedStatement pst = conn.prepareStatement(sql)) {
                pst.setInt(1, missionId);
                pst.executeUpdate();

                // No exceptions, success only if previous query was successful
                ret &= true;
            }
        } catch (SQLException e) {
            ret = false;
            if (printStackTrace) e.printStackTrace(); 
        }

        return ret;
    }

    // Returns largest mission id in the database
    // Returns 0 if there are no missions in the database
    // Returns -1 on error
    public int curMissionId() {
        int ret = -1;
        String sql = "SELECT MAX(mission_id) FROM mission";

        try (Connection conn = DriverManager.getConnection(url, user, password);) {
            try (PreparedStatement pst = conn.prepareStatement(sql)) {
                ResultSet rst = pst.executeQuery();

                if (rst.next()) {
                    ret = rst.getInt("MAX(mission_id)");
                } else {
                    ret = 0;
                }
            }
        } catch (SQLException e) {
            if (printStackTrace) e.printStackTrace(); 
        }

        return ret;
    }

    // Updates database to start a new mission,
    // using the next available mission id
    // Returns true on success, false on failure
    public boolean startMission(String incidentName) {
        boolean ret = false;
        String sql = "INSERT INTO mission (incident_name, mission_id, start_datetime, in_progress) VALUES (?,?,?,?)";
        LocalDate today = LocalDate.now();
        String now = LocalTime.now().truncatedTo(ChronoUnit.MINUTES).toString();
        String dateTime = today.getDayOfMonth() + " " + today.getMonth() + ", " + today.getYear() + "\n" + now;

        // Only start a mission if there is not one in progress
        Boolean mip = this.missionInProgress();
        if (mip == null || mip == true) {
            return false;
        }

        // Get the next mission id to use
        int missionId = curMissionId();
        if (missionId < 0) {
            return false;
        } else {
            missionId++;
        }

        try (Connection conn = DriverManager.getConnection(url, user, password);) {
            try (PreparedStatement pst = conn.prepareStatement(sql)) {
                pst.setString(1, incidentName);
                pst.setInt(2, missionId);
                pst.setString(3, dateTime);
                pst.setBoolean(4, true);
                pst.executeUpdate();

                // No exceptions, success
                ret = true;
            }
        } catch (SQLException e) {
            if (printStackTrace) e.printStackTrace(); 
        }

        return ret;   
    }

    // Updates the mission start date time for the current mission
    // Returns true on success, false on failure
    public boolean updateMissionStart(String startDateTime) {
        boolean ret = false;
        String sql = "UPDATE mission SET start_datetime=? WHERE in_progress=?";

        try (Connection conn = DriverManager.getConnection(url, user, password);) {
            try (PreparedStatement pst = conn.prepareStatement(sql)) {
                pst.setString(1, startDateTime);
                pst.setBoolean(2, true);
                pst.executeUpdate();
            }

            ret = true;
        } catch (SQLException e) {
            if (printStackTrace) e.printStackTrace(); 
        }

        return ret; 
    }

    // Updates the mission end datetime for the current mission
    // Returns true on success, false on failure
    public boolean updateMissionEnd(String endDateTime) {
        boolean ret = false;
        String sql = "UPDATE mission SET end_datetime=? WHERE in_progress=?";

        try (Connection conn = DriverManager.getConnection(url, user, password);) {
            try (PreparedStatement pst = conn.prepareStatement(sql)) {
                pst.setString(1, endDateTime);
                pst.setBoolean(2, true);
                pst.executeUpdate();
            }

            ret = true;
        } catch (SQLException e) {
            if (printStackTrace) e.printStackTrace(); 
        }

        return ret; 
    }

    // Ends the mission currently in progress and signs out all guests 
    // and members who are still signed in
    // Returns true on success, false on failure
    public boolean endMission() {
        boolean ret = false;
        String sql = "UPDATE mission SET in_progress=? WHERE in_progress=?";
        LocalDate today = LocalDate.now();
        String now = LocalTime.now().truncatedTo(ChronoUnit.MINUTES).toString();
        String dateTime = today.getDayOfMonth() + " " + today.getMonth() + ", " + today.getYear() + "\n" + now;

        try (Connection conn = DriverManager.getConnection(url, user, password);) {
            // End mission
            try (PreparedStatement pst = conn.prepareStatement(sql)) {
                pst.setBoolean(1, false);
                pst.setBoolean(2, true);
                pst.executeUpdate();
            }

            // Set mission end time if it has not been set
            sql = "UPDATE mission SET end_datetime=? WHERE end_datetime IS NULL";
            try (PreparedStatement pst = conn.prepareStatement(sql)) {
                pst.setString(1, dateTime);
                pst.executeUpdate();
            }

            // Sign out members
            sql = "UPDATE member_attendee SET signout_time=? WHERE signout_time=NULL";
            try (PreparedStatement pst = conn.prepareStatement(sql)) {
                pst.setString(1, now);
                pst.executeUpdate();
            }

            // Sign out guests
            sql = "UPDATE guest_attendee SET signout_time=? WHERE signout_time=NULL";
            try (PreparedStatement pst = conn.prepareStatement(sql)) {
                pst.setString(1, now);
                pst.executeUpdate();
            }

            ret = true;
        } catch (SQLException e) {
            if (printStackTrace) e.printStackTrace(); 
        }

        return ret; 
    }

    // Gets a list of all missions currently in the database
    // Returns null on failure
    public ArrayList<Mission> getAllMissions() {
        ArrayList<Mission> ret = null;
        ArrayList<Mission> temp = new ArrayList<Mission>();
        String sql = "SELECT * FROM mission";

        try (Connection conn = DriverManager.getConnection(url, user, password);) {
            try (PreparedStatement pst = conn.prepareStatement(sql)) {
                ResultSet rst = pst.executeQuery();

                // Fill list with missions
                while (rst.next()) {
                    Mission mis = new Mission();
                    mis.incidentName    = rst.getString("incident_name");
                    mis.missionId       = rst.getInt("mission_id");
                    mis.startDateTime   = rst.getString("start_datetime");
                    mis.endDateTime     = rst.getString("end_datetime");
                    mis.inProgress      = rst.getBoolean("in_progress");
                    temp.add(mis);
                }

                // No exception, return temp
                ret = temp;
            } 
        } catch (SQLException e) {
            if (printStackTrace) e.printStackTrace();
        }

        return ret;
    }

    // Returns true if a mission operational period is currently in progress
    // Returns false otherwise, and null on error
    public Boolean missionInProgress() {
        Boolean ret = null;
        String sql = "SELECT * FROM mission WHERE in_progress=?";

        try (Connection conn = DriverManager.getConnection(url, user, password);) {
            try (PreparedStatement pst = conn.prepareStatement(sql)) {
                pst.setBoolean(1, true);
                ResultSet rst = pst.executeQuery();

                // No exception, check if query returned anything
                if (rst.isBeforeFirst()) {
                    ret = true;
                } else {
                    ret = false;
                }
            } 
        } catch (SQLException e) {
            if (printStackTrace) e.printStackTrace(); 
        }

        return ret;      
    }
}
