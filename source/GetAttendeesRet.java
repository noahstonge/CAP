// Used to return multiple values from getAttendees model function
import java.util.List;

public class GetAttendeesRet {
	ModelState state;
	List<Person> attendees;
}