// Parent class for CAPMember and Guest, so they can be in the same lists

public class Person {
	String name;
	String ecName;
	String ecPhone;

	// Sign in information should be left uninitialized when not in use
	String signIn;
	String signOut;
}
