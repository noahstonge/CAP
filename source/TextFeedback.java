/* TextFeedback.java
 *
 * Text based user feedback for the barcode sign in station
 *
 */

public class TextFeedback implements Feedback {
    public TextFeedback () {
        // No attributes
    }

    public void comFailed() {
        System.out.println("Failed to communicate with database");
    }

    public void shutDown() {
        System.out.println("Exiting program");
    }

    public void startupSuccessful() {
        System.out.println("Program successfully started up");
    }

    public void badCAPId() {
        System.out.println("Invalid barcode");
    }

    public void notInDatabase() {
        System.out.println("CAP ID not in database");
    }

    public void signInSuccessful() {
        System.out.println("Signed in");
    }

    public void signOutSuccessful() {
        System.out.println("Signed out");
    }
}
