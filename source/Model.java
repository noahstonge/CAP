/* Model.java
 *
 * Class containing the model logic for the sign in system
 *
 */

import org.apache.pdfbox.pdmodel.*; 
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import java.io.File;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;


import java.util.List;
import java.util.ArrayList;

public class Model {
    DatabaseConnection dbct;

    public Model(DatabaseConnection dbct) {
        this.dbct = dbct;
    }

    /**
     * Checks to see if the connection with the database is functional
     * @return a ModelState indicating whether the connection is active
     */
    public ModelState checkConnection() {
        ModelState ret = ModelState.ERROR_UNKNOWN;
        if (dbct.checkConnection()) {
            ret = ModelState.SUCCESS;
        } else {
            ret = ModelState.ERROR_DB_COM_FAILED;
        }
        return ret;
    }

    /**
     * Checks to see if there is currently a mission in progress
     * @return a ModelState indicating whether a mission is in progress, or the appropriate error code
     */
    public ModelState isMissionInProgress() {
        ModelState ret = ModelState.ERROR_UNKNOWN;
        Boolean inProgress = dbct.missionInProgress();
        if (inProgress == null) {
            ret = ModelState.ERROR_DB_COM_FAILED;
        } else if (inProgress) {
            ret = ModelState.IN_PROGRESS;
        } else {
            ret = ModelState.NOT_IN_PROGRESS;
        }
        return ret;
    }

    /**
     * Starts a new mission
     * @param incidentName the name of the mission to be started
     * @return a ModelState indicating success or the appropriate error code
     */
    public ModelState startMission(String incidentName) {
        ModelState ret = ModelState.ERROR_UNKNOWN;

        ret = this.isMissionInProgress();
        if (ret == ModelState.IN_PROGRESS) {
            ret = ModelState.ERROR_ALREADY_IN_PROGRESS;
        } else if (ret == ModelState.NOT_IN_PROGRESS) {
            // Passed state checks, start the mission
            if (dbct.startMission(incidentName)) {
                ret = ModelState.SUCCESS;
            } else {
                ret = ModelState.ERROR_DB_COM_FAILED;
            }
        }
        
        return ret;
    }

    /**
     * Updates the starting datetime for the current mission
     * @param startDateTime the new starting datetime
     * @return a ModelState indicating success or the appropriate error code
     */
    public ModelState setMissionStart(String startDateTime) {
        ModelState ret = ModelState.ERROR_UNKNOWN;

        ret = this.isMissionInProgress();
        if (ret == ModelState.IN_PROGRESS) {
            if (dbct.updateMissionStart(startDateTime)) {
                ret = ModelState.SUCCESS;
            } else {
                ret = ModelState.ERROR_DB_COM_FAILED;
            }
        } else if (ret == ModelState.NOT_IN_PROGRESS) {
            ret = ModelState.ERROR_NO_MISSION_IN_PROGRESS;
        }

        return ret;
    }

    /**
     * Updates the ending datetime for the current mission
     * @param endDateTime the new ending datetime
     * @return a ModelState indicating success or the appropriate error code
     */
    public ModelState setMissionEnd(String endDateTime) {
        ModelState ret = ModelState.ERROR_UNKNOWN;

        ret = this.isMissionInProgress();
        if (ret == ModelState.IN_PROGRESS) {
            if (dbct.updateMissionEnd(endDateTime)) {
                ret = ModelState.SUCCESS;
            } else {
                ret = ModelState.ERROR_DB_COM_FAILED;
            }
        } else if (ret == ModelState.NOT_IN_PROGRESS) {
            ret = ModelState.ERROR_NO_MISSION_IN_PROGRESS;
        }

        return ret;
    }

    /**
     * Ends the current mission
     * @return a ModelState indicating success or the appropriate error code
     */
    public ModelState endMission() {
        ModelState ret = ModelState.ERROR_UNKNOWN;
        if (dbct.endMission()) {
            ret = ModelState.SUCCESS;
        } else {
            ret = ModelState.ERROR_DB_COM_FAILED;
        }
        return ret;
    }

    /**
     * Gets all missions currently in the database
     * @param retList an uninitialized list of missions where the missions will be stored
     * @return a GetAllMissionsRet containing a list of all missions and
     *  a ModelState indicating success or the appropriate error code
     */
    public GetAllMissionsRet getAllMissions() {
        GetAllMissionsRet ret = new GetAllMissionsRet();
        ret.state = ModelState.ERROR_UNKNOWN;
        ret.missions = dbct.getAllMissions();
        if (ret.missions == null) {
            ret.state = ModelState.ERROR_DB_COM_FAILED;
        } else {
            ret.state = ModelState.SUCCESS;
        }
        return ret;
    }

    /**
     * Deletes a mission from the database
     * @param m the mission to be deleted
     * @return a ModelState indicating success or the appropriate error code
     */
    public ModelState deleteMission(Mission m) {
        ModelState ret = ModelState.ERROR_UNKNOWN;
        if (dbct.removeMission(m.missionId)) {
            ret = ModelState.SUCCESS;
        } else {
            ret = ModelState.ERROR_DB_COM_FAILED;
        }
        return ret;
    }

    /**
     * Generates a form 211 for a given mission and saves it to 
     * @param m the mission to generate the form 211 for
     * @return a ModelState indicating success or the appropriate error code
     */
    public ModelState generateForm211(Mission m) {
        ModelState ret = ModelState.ERROR_UNKNOWN;

        // Get all attendees and check for failed communications
        List<CAPMember> members = dbct.getMissionMembers(m.missionId);
        List<Guest> guests = dbct.getMissionGuests(m.missionId);
        if (members == null || guests == null) {
            ret = ModelState.ERROR_DB_COM_FAILED;
        } else {
            // Put all attendees into a single list
            ArrayList<Person> allAttendees = new ArrayList<Person>();
            allAttendees.addAll(members);
            allAttendees.addAll(guests);

            if (allAttendees.size() == 0) {
                ret = ModelState.ERROR_NO_ATTENDEES;
            } else {
                ret = this.populateForm211(m, allAttendees);
            }
        }

        return ret;
    }

    /**
     * Gets all attendees for a past or currently in progress mission
     * @param m The mission to get the attendees for
     * @return a GetAttendeesRet containing a list of the attendees for the mission
     *  and a ModelState indicating success or the appropriate error code
     */
    public GetAttendeesRet getAttendees(Mission m) {
        GetAttendeesRet ret = new GetAttendeesRet();
        ret.state = ModelState.ERROR_UNKNOWN;
        List<CAPMember> members = dbct.getMissionMembers(m.missionId);
        List<Guest> guests = dbct.getMissionGuests(m.missionId);
        if (members == null || guests == null) {
            ret.state = ModelState.ERROR_DB_COM_FAILED;
        } else {
            ret.attendees = new ArrayList<Person>();
            ret.attendees.addAll(members);
            ret.attendees.addAll(guests);
            ret.state = ModelState.SUCCESS;
        }
        return ret;
    }

    /**
     * Adds a new CAPMember to the database
     * @param member the CAP member to add to the database
     * @return a ModelState indicating success or the appropriate error code
     */
    public ModelState addCAPMember(CAPMember member) {
        ModelState ret = ModelState.ERROR_UNKNOWN;
        if (validateCAPId(member.capId)) {
            if (dbct.addMember(member)) {
                ret = ModelState.SUCCESS;
            } else {
                ret = ModelState.ERROR_DB_COM_FAILED;
            }
        } else {
            ret = ModelState.ERROR_INVALID_CAP_ID;
        }
        return ret;
    }

    /**
     * Edits the information in the database for a given CAPMember
     * @param member the updated information for the CAPMember
     * @return a ModelState indicating success or the appropriate error code
     */
    public ModelState editCAPInfo(CAPMember member) {
        ModelState ret = ModelState.ERROR_UNKNOWN;
        if (validateCAPId(member.capId)) {
            if (dbct.editMember(member)) {
                ret = ModelState.SUCCESS;
            } else {
                ret = ModelState.ERROR_DB_COM_FAILED;
            }
        } else {
            ret = ModelState.ERROR_INVALID_CAP_ID;
        }
        return ret;
    }

    /**
     * Gets a CAPMember identified by their CAP ID from the database
     * @param id the CAP ID of the member to be fetched
     * @return a ModelState indicating success or the appropriate error code
     */
    public GetCAPMemberRet getCAPMember(String id) {
        GetCAPMemberRet ret = new GetCAPMemberRet();
        ret.state = ModelState.ERROR_UNKNOWN;
        if (validateCAPId(id)) {
            AttendeeStatus memberStatus = dbct.getMemberStatus(id);
            if (memberStatus == null) {
                ret.state = ModelState.ERROR_DB_COM_FAILED;
            } else if (!memberStatus.inDatabase) {
                ret.state = ModelState.ERROR_NOT_IN_DB;
            } else {
                ret.member = dbct.getMember(id);
                if (ret.member == null) {
                    ret.state = ModelState.ERROR_DB_COM_FAILED;
                } else {
                    ret.state = ModelState.SUCCESS;
                }
            }
        } else {
            ret.state = ModelState.ERROR_INVALID_CAP_ID;
        }
        return ret;
    }

    /**
     * Signs in a CAP Member with the given CAP ID
     * @param id the member's CAP ID number
     * @param inTime the sign in time for the CAP member
     * @return a ModelState indicating success or the appropriate error code
     */
    public ModelState CAPMemberSignIn(String id, String inTime) {
        ModelState ret = ModelState.ERROR_UNKNOWN;
        if (validateCAPId(id)) {
            AttendeeStatus memberStatus = dbct.getMemberStatus(id);
            if (memberStatus == null) {
                ret = ModelState.ERROR_DB_COM_FAILED;
            } else if (!memberStatus.inDatabase) {
                ret = ModelState.ERROR_NOT_IN_DB;
            } else {
                ModelState temp = isMissionInProgress();
                if (temp == ModelState.IN_PROGRESS) {
                    if (dbct.memberSignIn(id, inTime)) {
                        ret = ModelState.SUCCESS;
                    } else {
                        ret = ModelState.ERROR_DB_COM_FAILED;
                    }
                } else if (temp == ModelState.NOT_IN_PROGRESS) {
                    ret = ModelState.ERROR_NO_MISSION_IN_PROGRESS;
                } else {
                    ret = temp;
                }
            }
        } else {
            ret = ModelState.ERROR_INVALID_CAP_ID;
        }
        return ret;
    } 

    /**
     * Signs out a CAP Member with the given CAP ID
     * @param id the member's CAP ID number
     * @param outTime the sign out time for the CAP member
     * @return a ModelState indicating success or the appropriate error code
     */
    public ModelState CAPMemberSignOut(String id, String outTime) {
        ModelState ret = ModelState.ERROR_UNKNOWN;
        if (validateCAPId(id)) {
            if (dbct.memberSignOut(id, outTime)) {
                ret = ModelState.SUCCESS;
            } else {
                ret = ModelState.ERROR_DB_COM_FAILED;
            }
        } else {
            ret = ModelState.ERROR_INVALID_CAP_ID;
        }
        return ret;
    }

    /**
     * Signs a guest into the current mission
     * @param g the guest to sign in
     * @param inTime the sign in time for the guest
     * @return a ModelState indicating success or the appropriate error code
     */
    public ModelState guestSignIn(Guest g, String inTime) {
        ModelState ret = ModelState.ERROR_UNKNOWN;
        ModelState temp = isMissionInProgress();
        if (temp == ModelState.IN_PROGRESS) {
            if (dbct.addGuest(g, inTime)) {
                ret = ModelState.SUCCESS;
            } else {
                ret = ModelState.ERROR_DB_COM_FAILED;
            }
        } else if (temp == ModelState.NOT_IN_PROGRESS) {
            ret = ModelState.ERROR_NO_MISSION_IN_PROGRESS;
        } else {
            ret = temp;
        }
        return ret;
    } 

    /**
     * Signs a guest out of the current mission
     * @param g the guest to sign out
     * @param outTime the sign out time for the guest
     * @return a ModelState indicating success or the appropriate error code
     */
    public ModelState guestSignOut(Guest g, String outTime) {
        ModelState ret = ModelState.ERROR_UNKNOWN;
        if (dbct.guestSignOut(g.name, outTime)) {
            ret = ModelState.SUCCESS;
        } else {
            ret = ModelState.ERROR_DB_COM_FAILED;
        }
        return ret;
    }

    /**************************************************************************
     * PRIVATE HELPER FUNCTIONS
     * ************************************************************************/

    /**
     * Check that the CAP ID is a number with the correct number of digits
     * @param id the CAP ID to validate
     * @return True if valid, False if invalid
     */
    private boolean validateCAPId(String id) {
        boolean ret = false;

        // Cannot be null
        if (id != null) {
            // Must be 6 digits
            if (id.length() == 6) {
                // Convert CAP ID to integer
                try {
                    int capIDAsInt = Integer.parseInt(id);

                    // Both tests succeeded
                    ret = true;
                } catch (NumberFormatException e) {
                    // Failure
                }
            }
        }

        return ret;
    }

    /**
     * Generates a pdf Form 211 for the given mission. The output is named according to the mission
     * date and start time and is located in ../Form211Output
     * @param mission The mission for which to generate a Form 211
     * @param attendees List of all members and guests who were on the mission
     * @return A ModelState indicating success or failure
     */
    private ModelState populateForm211(Mission mission, ArrayList<Person> attendees) {
        final int numRows = 21;      // Number of attendees per page
        ModelState ret = ModelState.ERROR_UNKNOWN;

        // If the mission has not yet ended, then leave the end time to be manually filled
        String missionEndTime = "";
        if (mission.endDateTime != null) {
            missionEndTime = mission.endDateTime;
        }

        try (PDDocument finalDoc = new PDDocument();)
        {
            LocalDate today = LocalDate.now();
            String now = LocalTime.now().truncatedTo(ChronoUnit.MINUTES).toString();
            ArrayList<PDField> fields = new ArrayList<PDField>();
            int page = 1;
            int curRow = 1;

            while (attendees.size() > 0)
            {
                // Create objects for new page
                //PDDocument doc = new PDDocument().load(new ByteArrayInputStream(template));
                File file = new File("../Form211Output/Template/template.pdf"); 
                PDDocument doc = PDDocument.load(file); 
                PDDocumentCatalog docCatalog = doc.getDocumentCatalog();
                PDAcroForm acroForm = docCatalog.getAcroForm();

                // Populate header
                PDField incidentName = acroForm.getField("Incident_Name");
                incidentName.setValue(mission.incidentName);
                incidentName.setPartialName("Incident_Name_p" + page);
                fields.add(incidentName);
                PDField startTime = acroForm.getField("Start_Time");
                startTime.setValue(mission.startDateTime);
                startTime.setPartialName("Start_Time_p" + page);
                fields.add(startTime);
                PDField endTime = acroForm.getField("End_Time");
                endTime.setValue(missionEndTime);
                endTime.setPartialName("End_Time_p" + page);
                fields.add(endTime);

                PDField prepTime = acroForm.getField("Prep_Time");
                String prepStr = today.getDayOfMonth() + " " + today.getMonth() + ", " + today.getYear() + " / " + now;
                prepTime.setValue(prepStr);
                prepTime.setPartialName("Prep_Time" + page);
                fields.add(prepTime);


                // Populate rows
                for (int i = 1; i <= numRows; i++) {
                    if (attendees.size() == 0) {
                        break;
                    }
                    Person p = attendees.remove(0);

                    // Prepare fields
                    PDField name = acroForm.getField("Name_" + i);
                    name.setPartialName("Name_" + i + "p_" + page);
                    fields.add(name);
                    PDField id = acroForm.getField("CAPID_" + i);
                    id.setPartialName("CAPID" + i + "p_" + page);
                    fields.add(id);
                    PDField quals = acroForm.getField("Quals_" + i);
                    quals.setPartialName("Quals" + i + "p_" + page);
                    fields.add(quals);
                    PDField ec = acroForm.getField("EC_" + i);
                    ec.setPartialName("EC_" + i + "p_" + page);
                    fields.add(ec);
                    PDField inTime = acroForm.getField("In_" + i);
                    inTime.setPartialName("In_" + i + "p_" + page);
                    fields.add(inTime);
                    PDField outTime = acroForm.getField("Out_" + i);
                    outTime.setPartialName("Out_" + i + "p_" + page);
                    fields.add(outTime);

                    // Populate fields based on whether the person is a member or guest
                    name.setValue(p.name);
                    ec.setValue(p.ecName + " " + p.ecPhone);
                    inTime.setValue(p.signIn);
                    // If member was not signed out, use the mission end time
                    if (p.signOut == null) {
                        outTime.setValue("");
                    } else {
                        outTime.setValue(p.signOut);
                    }
                    if (p instanceof CAPMember) {
                        id.setValue(((CAPMember) p).capId);
                        quals.setValue(((CAPMember) p).qualifications);
                    } else {
                        id.setValue("Guest");
                        quals.setValue("N/A");
                    }
                }

                // Add page with fields populated to document
                PDPageTree pages = docCatalog.getPages();
                finalDoc.addPage(pages.get(0));
                page++;
            }

            PDAcroForm finalForm = new PDAcroForm(finalDoc);
            finalDoc.getDocumentCatalog().setAcroForm(finalForm);
            finalForm.setFields(fields);

            String name = "Form211_" + now + "_" + today.getDayOfMonth() + "_" + today.getMonth() + "_" + today.getYear() + ".pdf";
            finalDoc.save(new File("../Form211Output", name));

            // Successfully saved form 211
            ret = ModelState.SUCCESS;
        } catch (Exception e) {
            // Error occured during form 211 generation
            ret = ModelState.ERROR_FORM_GENERATION_FAILED;
            e.printStackTrace();
        }

        return ret;
    }

}
