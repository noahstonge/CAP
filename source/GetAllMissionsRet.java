// Used to return multiple values from Model.getAllMissions
import java.util.List;

public class GetAllMissionsRet {
	ModelState state;
	List<Mission> missions;
}