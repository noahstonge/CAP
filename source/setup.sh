#!/bin/bash

export CLASSPATH=./libraries/commons-logging-1.2.jar:$CLASSPATH
export CLASSPATH=./libraries/fontbox-2.0.24.jar:$CLASSPATH
export CLASSPATH=./libraries/mysql-connector-java-8.0.26.jar:$CLASSPATH
export CLASSPATH=./libraries/pdfbox-2.0.24.jar:$CLASSPATH
