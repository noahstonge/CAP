import java.net.*;
import java.time.*;
import java.text.SimpleDateFormat;
import java.io.IOException;
import java.util.*;
import java.lang.Math;
import javax.swing.*;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.BasicStroke;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.time.LocalDate;
import java.time.LocalTime;
import javax.swing.JOptionPane;
import java.time.format.DateTimeFormatter;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import java.util.List;
import java.text.DateFormat;  
import java.text.SimpleDateFormat;  
import java.util.Date;  
import java.util.Calendar;
import java.time.temporal.ChronoUnit;


public class runner{

public static GuiLayersListenersCAP gui;
private static int tmpInput = -1;
private static Queue<String> UserInputFromGui = new LinkedList<>();
private static JLabel[][] myLabels;/*[45][4] contains all text labels*/
private static JTextField[][] myFields;/*[45][2] contains all text fields*/
private static AccountUI g2;
private static runner x;
public static Boolean myStat = null;
private static Model model;
private static DateTimeFormatter fmtr = DateTimeFormatter.ofPattern("HH:mm:ss",Locale.US);
private static DateTimeFormatter dtfmtr = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
private static Boolean inProg = null;
//private static boolean creatingAccount = false;
   public static void main(String args[]){
      
      x = new runner();
      DatabaseConnection dbct = new DatabaseConnection("jdbc:mysql://localhost/cap_db","root","testing");
      model = new Model(dbct);
      gui.myR(x);
      
      gui.jCheckBox1.setEnabled(false);
      gui.jCheckBox2.setEnabled(false);
      gui.jCheckBox3.setEnabled(false);
      gui.jTextField1.setEnabled(false);
      gui.jTextField2.setEnabled(false);
      gui.jLabel4.setText("");
      
      
      
      
      myLabels = gui.getLabels();
      myFields = gui.getFields();
      if(!dbct.checkConnection()){
         lockForm();
         gui.jTextField3.setEditable(false);
         gui.jLabel10.setForeground(Color.RED);
         gui.jLabel10.setText("<html>Unable to connect to database</html>");
      }
      lockForm();
      //gui.jTextField6.setEditable(false);
      
      ModelState missionInProg = model.isMissionInProgress();
      if(missionInProg == ModelState.IN_PROGRESS){
         unlockForm();
         inProg = true;
         //gui.jTextField6.setEditable(false);
         //call function to populate the gui
         setupGuiForExistingMission();
         
      }
      else if(missionInProg == ModelState.ERROR_DB_COM_FAILED){
      }
      else if(missionInProg == ModelState.NOT_IN_PROGRESS){
         inProg = false;
      }
      
      //gui.jTextField6.setEditable(true);
      
      int count = 0;
      int input = 0;
      while(true){ //while application open
      //while we wait for the user input we thread sleep
      //once user updates it we break out and can continue on
         try {
            while(tmpInput < 0){
               try{
                  Thread.sleep(200);
               }
               catch(InterruptedException e){
               }
             }
             input = (int)tmpInput;
             tmpInput = -1;
          } catch (Exception e){
             input=0; // keep input invalid
          }
          //this is where we call handling code
          while(UserInputFromGui.peek() != null){
            String code = UserInputFromGui.remove();
            handleCode(code);
          }
      }
   
   }  
   //gets a single gui value
   public static void getGuiValue(String toSave){
      tmpInput = 1; //lets program know we hit an interrupted exception
      UserInputFromGui.add(toSave);
   }
   //used to break out of infinite loop then assigns the queue of strings to one in this class which is used to take input
   //from the gui
   public static void getManyGuiValues(Queue<String> a){
      UserInputFromGui = a;
      tmpInput=1;
   }
   
   //used to pass the gui object to this class
   public static void myPass(GuiLayersListenersCAP z){
      gui = z;
   }
   // sets g2 to the cap setupd window when we are setting up a cap account
   public static void myPass2(AccountUI zz){
      g2 = zz;
   }
   //this is a function used to setup a new cap account
   public static void capSet(){
      //creatingAccount = true;
      g2 = new AccountUI();
      g2.myR2(x);
      g2.setUp(g2);
      g2.setVisible(true);
      g2.jTextField1.requestFocus();
   }
   //create new guest and sign in
   public static void capSet2(){
      g2 = new AccountUI();
      g2.setGuest();
      g2.jTextField2.setVisible(false);
      g2.jTextField3.setVisible(false);
      //g2.jLabel3.setVisible(false);
      g2.jLabel3.setVisible(false);
      g2.jLabel2.setVisible(false);
      //g2.jTextField7.setVisible(false);
      //g2.jLabel9.setVisible(false);
      g2.myR2(x);
      g2.setUp(g2);
      g2.setVisible(true);
      g2.jTextField1.requestFocus();
   }
   public static void capSet3(){
      String capId = JOptionPane.showInputDialog(null, "Cap ID", null);
      GetCAPMemberRet ret = model.getCAPMember(capId);
      ModelState state = ret.state;
      CAPMember member = ret.member;
      if(state == ModelState.SUCCESS){
        g2 = new AccountUI();
        g2.myR2(x);
        g2.setUp(g2);
        g2.setEdit();
        //populate fields here
        g2.jTextField1.setText(member.name);
        g2.jTextField2.setText(member.capId);
        g2.jTextField3.setText(member.qualifications);
        g2.jTextField4.setText(member.ecName);
        g2.jTextField5.setText(member.ecPhone);
        g2.setVisible(true);
        g2.jTextField1.requestFocus(); 
      }
      else{
         JOptionPane.showMessageDialog(null, "Error could not find Cap ID in database");
      }
   }
   
   
   
   
   
   //this method is used to transmit the status of a function call to the gui
   //it waits until my stat is not null then it gives the variable to the gui
   //so it knows how to proceed
   public static Boolean getStatus(){
      while(myStat == null){
         try{
            Thread.sleep(1);
         }
         catch(InterruptedException e){}
      }
      return myStat;
   }
   public static void updateBool(){
      myStat = null;
   }
   //function that finds the next available index of text fields that is empty to edit
   public static int findOneToUpdate(){
      int toR = -1;
      for(int a=0;a<45;a++){
         if(myFields[a][0].getText().equals("") && myFields[a][0].isEditable()){
            toR = a;
            break;
         }
      }
      return toR;
   }
   
   public static int toSignOut(String capID){
      int toRet = -1;
      for(int a=0;a<45;a++){
         boolean x = myFields[a][0].getText().equals(capID);
         boolean y = myFields[a][1].getText().equals("");
         boolean z = myFields[a][1].getText().equals(" ");
         if(x && (y || z)){ //found cap id match and a sign out box not signed out at
            toRet = a;
            break;
         }
      }
      return toRet;
   }
   
   //iterates through all text fields, finds one that is not empty and is editable this only occurs when
   //a sign in attempt fails so we want to remove the cap id from the text box, set it to empty so it can be reused
   public static void findAndFixTextField(){
      for(int a=0;a<45;a++){
         if(myFields[a][0].getText()!= "" && myFields[a][0].isEditable()){
            myFields[a][0].setText("");
            break;
         }
      }
   }
   public static void lockForm(){
      for(int a=0;a<45;a++){
         for(int aa =0;aa<2;aa++){
            myFields[a][aa].setEditable(false);
         }
      }
      gui.jTextField6.setEditable(false);
      gui.jTextField7.setEditable(false);
   }
   
   public static void unlockForm(){
      for(int a=0;a<45;a++){
         for(int aa =0;aa<2;aa++){
            myFields[a][aa].setEditable(true);
         }
      }
      gui.jTextField6.setEditable(true);
      gui.jTextField7.setEditable(true);
   }
   
   public static void setupGuiForExistingMission(){
      Mission curMiss = getCurMission();
      GetAttendeesRet ret = model.getAttendees(curMiss);
      gui.jTextField3.setEditable(false);
      gui.jTextField3.setText(curMiss.incidentName);
      String date = curMiss.startDateTime;
      gui.jTextField6.setText(date);
      //start populating people who were signed in and those who still are
      List<Person> attendees = ret.attendees;
      for(int a=0;a<attendees.size();a++){
         Person tmp = attendees.get(a);
         String capIdPotentially = null;
         String quals = null;
         try{
            capIdPotentially = ((CAPMember)tmp).capId;
            quals = ((CAPMember)tmp).qualifications;
         }catch(ClassCastException e){}
         if(capIdPotentially == null){
            myFields[a][0].setText("Guest");
         }
         else{
            myFields[a][0].setText(capIdPotentially);
         }
         if(!(quals == null)){
            myLabels[a][1].setText(quals);
         }
         myFields[a][0].setEditable(false);
         if(tmp.signOut != null){
            myFields[a][1].setText(tmp.signOut.toString());
            myFields[a][1].setEditable(false);
         }
         else if(tmp.signOut == null){ 
            //this is the case where the person wasnt signed out of the mission so we leave it un signed out
         }
         myLabels[a][0].setText(tmp.name);
         myLabels[a][2].setText(tmp.ecName + " " + tmp.ecPhone);
         myLabels[a][3].setText(tmp.signIn.toString());
         
         //still need to get a model function to find the time they logged in and out and use that
         //to populate the fields next to them
      }
   }
   //gets the current mission
   public static Mission getCurMission(){
   /*
     Mission ret = null; 
     GetAllMissionsRet all = model.getAllMissions();
     if (all.state == ModelState.SUCCESS) {
         for (int i = 0; i < all.missions.size(); i++) {
             Mission m = all.missions.get(i);
             if (m.inProgress == true) {
                 ret = m;
             }
         }
     }
   
     return ret;
     */      
      
      Mission myM = null;
      GetAllMissionsRet mst = model.getAllMissions();
      List<Mission> m = mst.missions;
      myM = m.get(m.size() - 1);
      return myM;
      
   }
   //cleans the form and gets rid of all values on it when we generate a 211 form
   public static void clearForm(){
      gui.jTextField1.setText("");
      gui.jTextField2.setText("");
      gui.jTextField3.setText("");
      gui.jTextField6.setText("");
      gui.jTextField7.setText("");
      for(int k=0;k<45;k++){
         for(int o=0;o<4;o++){
            myLabels[k][o].setText("");
            if(o<2){
               myFields[k][o].setText("");
               myFields[k][o].setEditable(true);
            }
         }
      }
   }
   
   //takes in a code and will call the appropriate model functions, this is esentially the main
   //handling that does all the logic to determine what to do based on a code from the gui
   public static void handleCode(String code){
      if(code.equals("signIn")){
         int indexForTextField = Integer.parseInt(UserInputFromGui.remove());
         String capID = UserInputFromGui.remove();
         if(capID.equals("Guest")){
            Guest g = new Guest();
            indexForTextField = findOneToUpdate();
            g.name = UserInputFromGui.remove();
            g.ecName = UserInputFromGui.remove();
            g.ecPhone = UserInputFromGui.remove();
            
            String now = LocalTime.now().truncatedTo(ChronoUnit.MINUTES).toString();
            ModelState t = model.guestSignIn(g, now);
            
            if(t == ModelState.SUCCESS){
               myLabels[indexForTextField][0].setForeground(Color.BLACK);
               myLabels[indexForTextField][0].setText(g.name);
               myLabels[indexForTextField][2].setText(g.ecName + " " + g.ecPhone);
               myFields[indexForTextField][0].setText("Guest");
               myFields[indexForTextField][0].setEditable(false);
               myLabels[indexForTextField][3].setText(now);
               myStat = true;
            }
            else{
               myStat=false;
            }
            //call sign in function here
            //verify state for mystat
            //set this based on result from model to sign in guest
         }
         else{
               //set value of myStat depending on output from the model
               //if it fails we want to call this method to remove the cap id from jtextField and then make it editable again
               //also want to set myStat equal to result of if sign in worked, if it did work then in the gui we lock the text field
               //otherwise it stays unlocked which allows us to find it and remove info from it
             String now = LocalTime.now().truncatedTo(ChronoUnit.MINUTES).toString();
             ModelState stat = model.CAPMemberSignIn(capID, now);
             if(stat == ModelState.SUCCESS){
                GetCAPMemberRet myRet = model.getCAPMember(capID);
                myLabels[indexForTextField][0].setText(myRet.member.name);
                myLabels[indexForTextField][0].setForeground(Color.BLACK);
                String quals = myRet.member.qualifications;
                if(quals != null){
                  myLabels[indexForTextField][1].setText(quals);
                }
                
                myLabels[indexForTextField][2].setText(myRet.member.ecName + " " + myRet.member.ecPhone);
                LocalTime time = LocalTime.now();
                myLabels[indexForTextField][3].setText(now);
                myStat = true;
             }
             else if(stat == ModelState.ERROR_NOT_IN_DB){
                x.capSet();
                g2.jTextField3.setText(capID);
                myFields[indexForTextField][0].setText("");
                myStat = false;
             }
             else if(stat == ModelState.ERROR_INVALID_CAP_ID){
                myStat = false;
                myLabels[indexForTextField][0].setText("<html>Invalid CAP ID: " + capID + "</html>");
                myLabels[indexForTextField][0].setForeground(Color.RED);
             }
             else{
                myStat = false;
                myLabels[indexForTextField][0].setText("<html>Check internet connection</html>");
                myLabels[indexForTextField][0].setForeground(Color.RED);
             } 
         }   
      }
      else if(code.equals("signOut")){
         int indexForTextField = Integer.parseInt(UserInputFromGui.remove());
         String toCheck =  myFields[indexForTextField][0].getText();
         if(toCheck.equals("Guest")){
            Guest g = new Guest();

            g.name = myLabels[indexForTextField][0].getText();
            
            String now = myFields[indexForTextField][1].getText();
            ModelState stt = model.guestSignOut(g, now);
            if(stt==ModelState.SUCCESS){
               LocalDateTime dateTime = LocalDateTime.now();
               String myT = fmtr.format(dateTime);
               myFields[indexForTextField][1].setText(now);
               myFields[indexForTextField][1].setEditable(false);
               myStat = true;
            }
            else{
               myStat = false;
            }
         }
         else{
            String capID = UserInputFromGui.remove();

            if (capID.equals("")) { // Tried to sign out on a line with no one signed in, just discard and clear field
               myFields[indexForTextField][1].setText("");
               myStat = false;
            } else {              
                //set value of myStat depending on output from the model
                String now = myFields[indexForTextField][1].getText();//LocalTime.now().truncatedTo(ChronoUnit.MINUTES).toString();
                ModelState stat = model.CAPMemberSignOut(capID, now);
                if(stat == ModelState.SUCCESS){
                   myFields[indexForTextField][1].setText(now);
                   myFields[indexForTextField][1].setEditable(false);
                   myStat = true;
                }
                else{
                   myStat = false;
                }
             }
         }
         int fieldToUpdate = findOneToUpdate();
         myFields[fieldToUpdate][0].requestFocus(); 
      }
      else if(code.equals("createMission") && inProg != null && inProg == false){
         String missionName = UserInputFromGui.remove();
         if(missionName.equals("") || missionName.equals(" ") || missionName == null){
            gui.jTextField3.setEditable(true);
            gui.jTextField3.setText("");
         }
         else{
            ModelState stat = model.startMission(missionName);
            LocalDate today = LocalDate.now();
            String now = LocalTime.now().truncatedTo(ChronoUnit.MINUTES).toString();
            String dateTime = today.getDayOfMonth() + " " + today.getMonth() + ", " + today.getYear() + "\n" + now;
            gui.jTextField6.setText(dateTime);
            if(stat == ModelState.SUCCESS){
               gui.jTextField6.setEditable(false);
               unlockForm();
               inProg = true;
            }
            else{
               gui.jTextField3.setText("Unable to start mission");
            }
         }
      }
      else if(code.equals("endMission")){
         //set value of myStat depending on output from the model
         if (JOptionPane.showConfirmDialog(null, "Are you sure?", "WARNING", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            String missionName = UserInputFromGui.remove();
            ModelState stat = model.endMission();
            Mission myM = null;
            GetAllMissionsRet mst = model.getAllMissions();
            if(mst.state == ModelState.SUCCESS){
               List<Mission> m = mst.missions;
               myM = m.get(m.size() - 1);
               ModelState gen211 = model.generateForm211(myM);
               if(gen211 == ModelState.SUCCESS){
                  JOptionPane.showMessageDialog(null,"Form 211 generation completed, form can be found in Form211Output folder","",1);
                  //moved these from was here to this location
                  gui.jTextField3.setText("");
                  
                  clearForm();
                  inProg = false;
                  lockForm();
                  gui.jTextField3.setEditable(true);
               }
               else{
                  JOptionPane.showMessageDialog(null,"Error generating 211 please try again later","",1);
               }
            }
            else{
            }
         }
         else {//no option
         }
      }
      else if(code.equals("createCapAccount")){
         //set value of myStat to the return from the database success
         CAPMember member = new CAPMember();
         member.name = UserInputFromGui.remove();
         member.capId = UserInputFromGui.remove();
         member.qualifications = UserInputFromGui.remove();
         member.ecName = UserInputFromGui.remove();
         member.ecPhone = UserInputFromGui.remove();
         
         ModelState stat = model.addCAPMember(member);
         if(stat == ModelState.SUCCESS){
            myStat = true;
         }
         else if(stat==ModelState.ERROR_INVALID_CAP_ID){
            myStat = false;
         }
         else if(stat==ModelState.ERROR_ALREADY_IN_DB){
            myStat = false;
         }
         else{
            myStat = false;
         }
         gui.processingCapId = false;
         //sign in cap account
         int nextAvail = findOneToUpdate();
         if(nextAvail != -1){
            String now = LocalTime.now().truncatedTo(ChronoUnit.MINUTES).toString();
            ModelState stat2 = model.CAPMemberSignIn(member.capId, now);
             if(stat2 == ModelState.SUCCESS){
                myLabels[nextAvail][0].setText(member.name);
                myLabels[nextAvail][0].setForeground(Color.BLACK);
                String quals = member.qualifications;
                if(member.qualifications != null){
                  myLabels[nextAvail][1].setText(member.qualifications);
                }
                
                myLabels[nextAvail][2].setText(member.ecName + " " + member.ecPhone);
                myLabels[nextAvail][3].setText(now);
                //myStat = true; Dont think i want to do this here
                myFields[nextAvail][0].setText(member.capId);
                myFields[nextAvail][0].setEditable(false);
                if(nextAvail+1 < myFields.length){
                  myFields[nextAvail+1][0].requestFocus();
                }
             }
         }
      }
      else if(code.equals("capId")){
         int indexForTextField = Integer.parseInt(UserInputFromGui.remove());
         String capID = UserInputFromGui.remove();
         GetCAPMemberRet stat = model.getCAPMember(capID);
         CAPMember member = stat.member;
         ModelState state = stat.state;
         myFields[indexForTextField][1].setText("");
         if(!myFields[indexForTextField][0].isEditable()){ //if trying to sign in or out where someone already signed in we find new index for the new sign in/out
            indexForTextField = findOneToUpdate();
         }
         if(state == ModelState.ERROR_DB_COM_FAILED){
            myLabels[indexForTextField][0].setText("<html>Check internet connection</html>");
            myLabels[indexForTextField][0].setForeground(Color.RED);
            myFields[indexForTextField][0].setText("");
            gui.processingCapId = false;
         }
         else if(state == ModelState.ERROR_INVALID_CAP_ID){
            myLabels[indexForTextField][0].setText("<html>Invalid CAP ID: " + capID + "</html>");
            myLabels[indexForTextField][0].setForeground(Color.RED);
            myFields[indexForTextField][0].setText("");
            gui.processingCapId = false;
         }
         else if(state == ModelState.ERROR_NOT_IN_DB){
            x.capSet();
            g2.jTextField2.setText(capID);
            myFields[indexForTextField][0].setText("");
            myFields[indexForTextField][1].setText("");
         }
         else if(state == ModelState.SUCCESS && (toSignOut(capID) == -1)/*member.signIn == null*/){
           String now = LocalTime.now().truncatedTo(ChronoUnit.MINUTES).toString();
           ModelState stat2 = model.CAPMemberSignIn(capID, now);
             if(stat2 == ModelState.SUCCESS){
                GetCAPMemberRet myRet = model.getCAPMember(capID);
                myLabels[indexForTextField][0].setText(myRet.member.name);
                myLabels[indexForTextField][0].setForeground(Color.BLACK);
                String quals = myRet.member.qualifications;
                if(quals != null){
                  myLabels[indexForTextField][1].setText(quals);
                }
                
                myLabels[indexForTextField][2].setText(myRet.member.ecName + " " + myRet.member.ecPhone);
                myLabels[indexForTextField][3].setText(now);
                myFields[indexForTextField][0].setText(capID);
                myFields[indexForTextField][1].setText("");
                myFields[indexForTextField][0].setEditable(false);
                if(indexForTextField + 1 < myFields.length){
                  myFields[indexForTextField + 1][0].requestFocus();
                }
                gui.processingCapId = false;
             }
         }
         else if(state == ModelState.SUCCESS){
             String now = LocalTime.now().truncatedTo(ChronoUnit.MINUTES).toString();
             ModelState stat2 = model.CAPMemberSignOut(capID, now);
             if(stat2 == ModelState.SUCCESS){
                //LocalDateTime dateTime = LocalDateTime.now();
                String myT = LocalTime.now().truncatedTo(ChronoUnit.MINUTES).toString();
                int fieldToUpdate;// = findCapId(capID, indexForTextField);
                fieldToUpdate = toSignOut(capID);
                myFields[fieldToUpdate][1].setText(myT);
                myFields[indexForTextField][0].setText("");//remove lingering old data from gui
                myFields[fieldToUpdate][1].setEditable(false);
                fieldToUpdate = findOneToUpdate();
                myFields[fieldToUpdate][0].requestFocus(); 
                gui.processingCapId = false;
             }
         }
         
      }
      else if(code.equals("EditMissionStart")){//call model code here
         UserInputFromGui.remove();
         ModelState stat2 = model.setMissionStart(UserInputFromGui.remove());
      }
      else if(code.equals("editCap")){
         CAPMember member = new CAPMember();
         member.name = UserInputFromGui.remove();
         member.capId = UserInputFromGui.remove();
         member.qualifications = UserInputFromGui.remove();
         member.ecName = UserInputFromGui.remove();
         member.ecPhone = UserInputFromGui.remove();
         ModelState state = model.editCAPInfo(member);
         if(state == ModelState.SUCCESS){
            myStat = true;
         }
         else{
            myStat = false;
         }
      }
      else if(code.equals("gen211NoEndMission")){
         Mission cur = getCurMission();
         ModelState state = model.generateForm211(cur);
         if((state == ModelState.SUCCESS)){
            JOptionPane.showMessageDialog(null, "Form 211 generation completed");
         }
         else{
            JOptionPane.showMessageDialog(null, "Unable to generate form 211 due to database connection");
         }
      }
      else if(code.equals("updateEndTime")){
         ModelState stat = model.setMissionEnd(gui.jTextField7.getText());
      }
   }
   
   public static int findCapId(String capId, int scanInd){
      for(int i=0;i<myFields.length;i++){
         if(myFields[i][0].getText().equals(capId) && scanInd != i){
            return i;
         }
      }
      return -1;
   }
   
   public static int findCapIdAndSignedIn(String capId){
      for(int i=0;i<myFields.length;i++){
         if(myFields[i][0].getText().equals(capId) && myFields[i][1].equals("")){
            return i;
         }
      }
      return -1;
   }
   
   
}
